<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'facebook' => [
        'client_id' => '429873488677680',
        'client_secret' => 'e2e1fa7df163329f8a6a46ac7495fab6',
        'redirect' => 'http://localhost:8000/callback',
    ],

    'google' => [
        'client_id' => '901808968416-edd40qv3sq4dihq3pllgpuk9dk64un33.apps.googleusercontent.com',
        'client_secret' => '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',
        'redirect' => 'http://localhost:8000/authorized/google/callback',
    ],

];
