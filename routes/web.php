<?php

use App\Http\Livewire\Home;
use App\Http\Livewire\PostCrud;
use App\Http\Livewire\PembeliCrud;
use App\Http\Livewire\PelaporanCrud;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginWithFacebookController;
use App\Http\Controllers\LoginWithGoogleController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('authorized/google', [LoginWithGoogleController::class, 'redirectToGoogle']);
Route::get('authorized/google/callback', [LoginWithGoogleController::class, 'handleGoogleCallback']);
Route::get('/redirect', [LoginWithFacebookController::class, 'redirectFacebook']);
Route::get('/callback', [LoginWithFacebookController::class, 'facebookCallback']);

Route::get('/', Home::class)->name('/');

Route::middleware(['auth:sanctum', 'verified'])->group(function () {
    Route::get('posts', PostCrud::class)->name('posts');
    Route::get('pembelis', PembeliCrud::class)->name('pembelis');
    Route::get('pelaporans', PelaporanCrud::class)->name('pelaporans');
    Route::get('/dashboard', function () {
        return view('dashboard');
    })->name('dashboard');
}); 

// Route::get('/booking', function () {
//     return view('booking')
// });

Route::view('/booking', 'layouts.booking');
Route::view('/profile', 'layouts.profile');

Route::view('/booking-futsal', 'layouts.booking-futsal');

// Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
//     return view('dashboard');
// })->name('dashboard');




Route::get('redirects', 'App\Http\Controllers\HomeController@index');


